import config

app = config.swagger
app.add_api('beasts.yaml')

@app.route('/')
def home():
    return 'Beasts API'

if __name__=='__main__':
    app.run()