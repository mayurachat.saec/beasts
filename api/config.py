import connexion
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

swagger = connexion.App(__name__, specification_dir='./')

app = swagger.app

SQLALCHEMY_DATABASE_URI = "mysql+mysqlconnector://{username}:{password}@{hostname}/{databasename}".format(
    username="mayurachatsaecha",
    password="Fah0824331513",
    hostname="mayurachatsaechan.mysql.pythonanywhere-services.com",
    databasename="mayurachatsaecha$beasts",
)

app.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI
app.config['SQLALCHEMY_POOL_RECYCLE'] = 299
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['SQLALCHEMY_ECHO'] = True

CORS(app)

db = SQLAlchemy(app)

ma = Marshmallow(app)