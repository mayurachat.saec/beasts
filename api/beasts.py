from flask import jsonify, abort
from config import db
from models import Beast, BeastSchema
from sqlalchemy.sql import text

def update(beast_id, beast):
    update_beast = Beast.query.filter(Beast.id == beast_id).one_or_none()

    name = beast.get("name")
    existing_beast = (Beast.query.filter(Beast.name == name).one_or_none())

    if (update_beast is None):
        abort(404, "Beast not found for Id: {beast_id}".format(beast_id=beast_id))

    elif (existing_beast is not None and existing_beast.id != beast_id):
        abort(409, "Beast {name} already exists".format(name=name))

    else:
        schema = BeastSchema()
        update = schema.load(beast, session=db.session)
        update.id = update_beast.id
        db.session.merge(update)
        db.session.commit()

        data = schema.dump(update_beast)
        return data, 200

BEASTS = {
    "Niffler": {
        "name": "Niffler",
        "desc": "A Niffler was a creature with a long snout and a coat of black, fluffy fur. They were attracted to shiny things, which made them wonderful",
        "image": ""
    },
    "Fairy": {
        "name": "Fairy",
        "desc": "A Fairy is a small human-like creature with large insect-like wings, which are either transparent or multi-coloured. They possess diminuti",
        "image": ""
    },
    "Chimaera": {
        "name": "Chimaera",
        "desc": "The Chimaera is a vicious, bloodthirsty creature with a lion's head, a goat's body and a dragon's tail",
         "image": ""
    }
}

def checkdb():
    try:
        results = db.session.query('1').from_statement(text('SELECT 1')).all()
        return jsonify({"Database connection": results})
    except:
        return jsonify({"Database connection": "Failed"})

#def all():
    #return jsonify(BEASTS)

def all():
    beast = Beast.query \
        .order_by(Beast.id) \
        .all()

    beast_schema = BeastSchema(many=True)
    data = beast_schema.dump(beast)
    return data

# Create beasts
def create(beast):
    name = beast.get("name")
    existing_beast = (Beast.query.filter(Beast.name == name).one_or_none())

    # Add new beast
    if existing_beast is None:
        schema = BeastSchema()
        new_beast = schema.load(beast, session=db.session)
        db.session.add(new_beast)
        db.session.commit()

        data = schema.dump(new_beast)
        return data, 201
    # Duplicate beast
    else:
        abort(409, "Beast {name} already exists".format(name=name))
